import { Math } from 'phaser'
import { ACTION_CONFIG, CONTROLS_CONFIG } from './config'
import Projectile from './projectile'
import Entity, { EntityProperties } from './entity'

export type PlayerProperties = EntityProperties & { damage: number }

export default class Player extends Entity {
  shootCooldown: boolean
  previousPosition: Math.Vector2

  damage: number

  constructor(properties: PlayerProperties) {
    super(properties)
    
    this.shootCooldown = false

    this.previousPosition = new Math.Vector2(properties.spawnPosition.x, properties.spawnPosition.y)
    this.damage = properties.damage

    this.scene.physics.add.collider(
      this,
      properties.scene.enemies,
      (source, target) => {
        const player = source as any
        const enemy = target as any

        const isPlayerOnTop = player.y < enemy.y - 16

        if(isPlayerOnTop) {
          enemy.takeDamage(player.damage)
          this.body.velocity.y = -this.velocity.y * 15
        }
        else {
          player.takeDamage(enemy.damage)
          this.body.velocity.y = (player.x > enemy.x ? 1 : -1) * this.velocity.x * 15
        }
      }
    )

    this.scene.physics.add.collider(
      this,
      this.map.traps,
      (source, target) => {
        const player = source as any

        player.takeDamage(10)
        this.body.velocity.y = -this.velocity.y * 15
      }
    )

    this.scene.physics.add.overlap(this, this.map.pickups, 
      (source, target) => {
        const player = source as any
        const pickup = target as any

        player.addHealth(10)
        pickup.destroy()
      }
    )
  }

  update(inputDirection: Math.Vector2, inputActions: any, deltaTime: number) {
    const healthElement = document.getElementById('health') as HTMLSpanElement
    healthElement.innerText = 'health: ' + Math.RoundTo(this.health, 0).toString()

    if(this.health <= 0) {
      healthElement.innerText = 'health: 0'
      return
    }

    if(inputDirection.x !== 0) {
        this.body.velocity.x = inputDirection.x * (CONTROLS_CONFIG.velocity.x * (this.body.blocked.down ? 1.0 : CONTROLS_CONFIG.airControlFactor)) * deltaTime
        this.direction = inputDirection.x
        this.setFlipX(this.direction < 0)
    }
    else
        this.body.velocity.x = 0

    if(inputDirection.y < 0 && this.body.blocked.down)
        this.body.velocity.y = inputDirection.y * CONTROLS_CONFIG.velocity.y * deltaTime

    if(this.body.velocity.x !== 0 || this.body.velocity.y !== 0) {
      const debugLine = this.scene.add.line(0, 0, this.previousPosition.x, this.previousPosition.y, this.x, this.y, 0xff00ff)
      setTimeout(() => {
        debugLine.destroy()
      }, 1000)

      this.previousPosition.x = this.x
      this.previousPosition.y = this.y
    }

    if(inputActions.shooting && !this.shootCooldown) {
      this.shootCooldown = true

      this.scene.add.existing(new Projectile({ scene: this.scene, damage: 10, velocity: 800, textureName: 'fireball', direction: this.direction, map: this.map, spawnPosition: new Math.Vector2(this.x, this.y)}))

      setTimeout(() => {
        this.shootCooldown = false
      }, ACTION_CONFIG.shootCooldownTime)
    }
  }
}