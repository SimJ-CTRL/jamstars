import { Scene, Tilemaps } from 'phaser'

export type MapSettings = { tilemapKey: string, scene: Scene }

type TilemapLayer = 'foreground'
type ObjectLayer = 'entries' | 'exits' | 'health' | 'spikes'
enum ObjectLayerKey {
  ENTRIES = 'entries',
  EXITS = 'exits',
  HEALTH = 'health',
  SPIKES = 'spikes'
}

export default class Map {
  objectLayer: { [key in ObjectLayer]: Tilemaps.ObjectLayer }
  tilemapLayer: { [key in TilemapLayer]: Tilemaps.TilemapLayer }

  pickups: Array<Phaser.Physics.Arcade.Image>
  traps: Array<Phaser.Physics.Arcade.Image>

  constructor({ tilemapKey, scene }: MapSettings) {
    const map = scene.make.tilemap({ key: tilemapKey, tileWidth: 64, tileHeight: 64 })

    const foregroundTileset = map.addTilesetImage('foreground', 'foreground')

    this.objectLayer = { 
      entries: map.getObjectLayer(ObjectLayerKey.ENTRIES), 
      exits: map.getObjectLayer(ObjectLayerKey.EXITS),
      health: map.getObjectLayer(ObjectLayerKey.HEALTH),
      spikes: map.getObjectLayer(ObjectLayerKey.SPIKES),
    }

    this.tilemapLayer = {
      foreground: map.createLayer(0, foregroundTileset, 0, 0)
    }

    map.setCollision([ 1, 2, 3, 6, 7 ], true, true, this.tilemapLayer.foreground, true)

    this.pickups = []
    for(const healthObject of this.objectLayer.health.objects) { //@ts-ignore
      const healthPickup = scene.physics.add.image(healthObject.x + 32, healthObject.y - 32, "carrot")
      
      healthPickup.setImmovable(true)
      healthPickup.body.setAllowGravity(false)

      this.pickups.push(healthPickup)
    }

    this.traps = []
    for(const spikeObject of this.objectLayer.spikes.objects) { //@ts-ignore
      const spikeTrap = scene.physics.add.image(spikeObject.x + 32, spikeObject.y - 32, "spike")
      
      spikeTrap.setImmovable(true)
      spikeTrap.body.setAllowGravity(false)

      this.traps.push(spikeTrap)
    }
  }
}