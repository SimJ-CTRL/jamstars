import * as Phaser from 'phaser';
import { CONTROLS_CONFIG, ENEMIES, IMAGES, INPUT_ACTION_CONFIG, INPUT_CONTROLS_CONFIG, PLAYER_CONFIG } from './config';
import { GAME_NAME, GRAVITY, IMAGE_BASE_PATH, SCREEN_SIZE } from './globals';
import Map from './map';
import Player from './player';
import Enemy from './enemy';

type InputControlsBinding = {[name: string]: {key: Phaser.Input.Keyboard.Key, axis: any, direction: -1 | 1}}
type InputActionBinding = {[name: string]: {key: Phaser.Input.Keyboard.Key}}

export default class CustomScene extends Phaser.Scene
{
    inputControlsBinding: InputControlsBinding
    inputActionBinding: InputActionBinding

    player: any | Player
    enemies: any | Enemy[]

    map: null | Map

    constructor ()
    {
        super(GAME_NAME)

        this.inputControlsBinding = {}
        this.inputActionBinding = {}

        this.player = null
        this.enemies = []

        this.map = null
    }

    preload ()
    {
        // bind input based on config
        for(const [key, value] of Object.entries(INPUT_CONTROLS_CONFIG))
            this.inputControlsBinding[key] = {...value, key: this.input.keyboard.addKey(value.key)}
        for(const [key, value] of Object.entries(INPUT_ACTION_CONFIG))
            this.inputActionBinding[key] = {key: this.input.keyboard.addKey(value.key)}

        for(const [key, value] of Object.entries(IMAGES))
            this.load.image(key, IMAGE_BASE_PATH + value)

        this.load.image('foreground', 'assets/tilesets/foreground.png')
        this.load.image('objects', 'assets/tilesets/objects.png')
        
        this.load.tilemapTiledJSON('map1', 'assets/tilemaps/map1.json')
    }

    create () {
        this.map = new Map({tilemapKey: 'map1', scene: this})
        
        this.player = this.add.existing(new Player({...CONTROLS_CONFIG, ...PLAYER_CONFIG, spawnPosition: new Phaser.Math.Vector2(256, 256), scene: this, map: this.map}))   
        this.cameras.main.startFollow(this.player)

        let distance = 512
        for(const enemy of Object.values(ENEMIES)) {
            this.enemies.push(this.add.existing(new Enemy({...enemy, spawnPosition: new Phaser.Math.Vector2(distance, 128), scene: this, map: this.map, player: this.player})))
            distance += 512
        }
    }

    update(time: number, deltaTime: number) {
        const getInputDirection = (keyboard: any, inputControlsBinding: InputControlsBinding) => {
            const inputDirection = new Phaser.Math.Vector2(0, 0)
            
            for(const {key, axis, direction} of Object.values(inputControlsBinding)) //@ts-expect-error
                if(keyboard.checkDown(key)) inputDirection[axis] += direction

            return inputDirection
        }
        const getInputActions = (keyboard: any, inputActionBinding: InputActionBinding) => {
            const inputActions = {}
            
            for(const [name, {key}] of Object.entries(inputActionBinding)) //@ts-expect-error
                inputActions[`${name}ing`] = keyboard.checkDown(key)

            return inputActions
        }

        this.player.update(getInputDirection(this.input.keyboard, this.inputControlsBinding), getInputActions(this.input.keyboard, this.inputActionBinding), deltaTime)
        this.enemies.forEach((enemy: Enemy) => enemy.update(deltaTime))
    }
}

const CONFIG = Object.freeze({
    type: Phaser.AUTO,
    backgroundColor: '#aaaaaa',
    width: SCREEN_SIZE.width,
    height: SCREEN_SIZE.height,
    scene: CustomScene,
    antialias: false,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: GRAVITY }
        }
    },
})

const game = new Phaser.Game(CONFIG)